from django.http import JsonResponse
from django.urls import reverse
from .models import Presentation
from django.forms.models import model_to_dict




def api_list_presentations(request, conference_id):
    presentations = [
        {
            "title": p.title,
            "status": p.status.name,
            "href": p.get_api_url(),
        }
        for p in Presentation.objects.filter(conference=conference_id)
    ]
    return JsonResponse({"presentations": presentations})


def api_show_presentation(request, id):
    try:
        presentation = Presentation.objects.get(pk=id)
    except Presentation.DoesNotExist:
        return JsonResponse({"error": "Presentation not found"}, status=404)


    presentation_dict = model_to_dict(presentation)


    presentation_dict.update({
        "status": presentation.status.name,
        "conference": {
            "name": presentation.conference.name,
            "href": request.build_absolute_uri(reverse("api_show_conference", kwargs={"id": presentation.conference.id}))
        },
        "created": presentation.created.strftime('%Y-%m-%d %H:%M:%S'),
    })

    return JsonResponse(presentation_dict)
