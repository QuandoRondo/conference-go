from json import JSONEncoder


class ModelEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, db.Model):
            return obj.to_dict()
        return JSONEncoder.default(self, obj)
