"""conference_go URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from events.api_views import api_show_conference, api_show_location, api_list_locations
from presentations.api_views import api_list_presentations, api_show_presentation
from attendees.api_views import api_list_attendees

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/attendees/", include("attendees.api_urls")),
    path("api/events/", include("events.api_urls")),
    path("api/conferences/<int:conference_id>/attendees/", api_list_attendees, name="api_list_attendees"),
    path("api/presentations/<int:id>/", api_show_presentation, name='api_show_presentation'),
    path("api/presentations/", api_list_presentations, name='api_list_presentations'),
    path("api/conferences/<int:conference_id>/presentations/", api_list_presentations, name='api_list_presentations'),
    path("api/locations/<int:id>/", api_show_location, name='api_show_location'),  
    path("api/locations/", api_list_locations, name='api_list_locations'),
    path("api/events/<int:id>/", api_show_conference, name='api_show_event'),
    path("api/conferences/<int:id>/", api_show_conference, name='api_show_conference'),
]
