import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_pexels_photo_url(city, state):

    pexels_url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": f"{city} {state}", "per_page": 1}

    response = requests.get(pexels_url, headers=headers, params=params)

    if response.status_code == 200:
        data = response.json()
        photo_url = data.get("photos", [{}])[0].get("src", {}).get("original")
        return photo_url
    else:

        print(f"Error getting Pexels photo: {response.status_code}")
        return None


import requests

def get_open_weather_data(latitude, longitude, api_key):
    base_url = 'https://api.openweathermap.org/data/2.5/weather'
    params = {
        'lat': latitude,
        'lon': longitude,
        'appid': api_key,
    }

    response = requests.get(base_url, params=params)

    if response.status_code == 200:
        weather_data = response.json()
        return weather_data
    else:
        print(f"Weather API request failed with status code: {response.status_code}")
        return None


api_key = OPEN_WEATHER_API_KEY
latitude = 40.7128
longitude = -74.0060

weather_data = get_open_weather_data(latitude, longitude, api_key)

if weather_data:
    print("Weather Data:", weather_data)
