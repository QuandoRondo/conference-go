from django.http import JsonResponse
from django.urls import reverse
from .models import Conference, Location
from django.forms.models import model_to_dict
from common.json import ModelEncoder
from .acls import get_pexels_photo_url, get_open_weather_data
from .utils import get_open_weather_api_key
from django.conf import settings

import requests

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "starts",
        "ends",
        "description",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
    ]




def get_open_weather_data(city, state, api_key):
    try:
        base_url = 'https://api.openweathermap.org/data/2.5/weather'
        response = requests.get(f'{base_url}?q={city},{state}&appid={api_key}')

        if response.status_code == 200:
            weather_data = response.json()
            temperature = weather_data.get('main', {}).get('temp')
            description = weather_data.get('weather', [{}])[0].get('description')
            return {"temp": temperature, "description": description}
        else:
            print(f"Weather API request failed with status code: {response.status_code}")
            return None

    except Exception as e:
        print(f"Error fetching weather data: {str(e)}")
        return None

    
    if weather_info:
        location_data["weather"] = weather_info

    return JsonResponse(location_data)


def api_list_conferences(request):

    response = []
    conferences = Conference.objects.all()
    for conference in conferences:
        response.append(
            {
                "name": conference.name,
                "href": conference.get_api_url(),
            }
        )
    return JsonResponse({"conferences": response})


def api_show_conference(request, id):

    conference = Conference.objects.get(id=id)

    weather_info = get_open_weather_data(conference.location.city, conference.location.state)
    print("Weather Info for Conference:", weather_info)


    picture_url = get_pexels_photo_url(conference.location.city, conference.location.state)

    return JsonResponse(
        {
            "name": conference.name,
            "starts": conference.starts,
            "ends": conference.ends,
            "description": conference.description,
            "created": conference.created,
            "updated": conference.updated,
            "max_presentations": conference.max_presentations,
            "max_attendees": conference.max_attendees,
            "location": {
                "name": conference.location.name,
                "href": conference.location.get_api_url(),
                "picture_url": picture_url,
            },
        }
    )


def api_list_locations(request):
    locations = Location.objects.all()

    locations_list = []

    for location in locations:
        picture_url = get_pexels_photo_url(location.city, location.state)

        location_dict = {
            "name": location.name,
            "href": location.get_api_url(),
            "picture_url": picture_url,
        }

        locations_list.append(location_dict)

    return JsonResponse({"locations": locations_list})




def get_open_weather_data(city, state):
    try:
        api_key = get_open_weather_api_key()
        base_url = 'https://api.openweathermap.org/data/2.5/weather'
        response = requests.get(f'{base_url}?q={city},{state}&appid={api_key}')

        if response.status_code == 200:
            weather_data = response.json()
            temperature = weather_data.get('main', {}).get('temp')
            description = weather_data.get('weather', [{}])[0].get('description')
            return {"temp": temperature, "description": description}
        else:
            print(f"Weather API request failed with status code: {response.status_code}")
            return None

    except Exception as e:
        print(f"Error fetching weather data: {str(e)}")
        return None


def api_show_location(request, id):
    location = Location.objects.get(id=id)


    weather_info = get_open_weather_data(location.city, location.state)
    print("Weather Info for Location:", weather_info)

    picture_url = get_pexels_photo_url(location.city, location.state)


    location_data = {
        "name": location.name,
        "city": location.city,
        "room_count": location.room_count,
        "created": location.created,
        "updated": location.updated,
        "state": {
            "name": location.state.name,
            "abbreviation": location.state.abbreviation,
        },
        "conferences": [
            {
                "name": conference.name,
                "starts": conference.starts,
                "ends": conference.ends,
                "description": conference.description,
                "created": conference.created,
                "updated": conference.updated,
                "max_presentations": conference.max_presentations,
                "max_attendees": conference.max_attendees,
                "location": {
                    "name": conference.location.name,
                    "href": conference.location.get_api_url(),
                },
            }
            for conference in location.conferences.all()
        ],
        "picture_url": picture_url,
    }


    if weather_info:
        location_data["weather"] = weather_info

    return JsonResponse(location_data)
