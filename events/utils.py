from django.conf import settings

def get_open_weather_api_key():
     return settings.OPEN_WEATHER_API_KEY
