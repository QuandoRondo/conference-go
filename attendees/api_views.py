from django.http import JsonResponse
from django.urls import reverse
from .models import Attendee


def api_list_attendees(request, conference_id):
    attendees = Attendee.objects.filter(conference_id=conference_id)

    attendees_list = [
        {
            "name": attendee.name,
            "href": reverse("api_show_attendee", args=[attendee.id]),
        }
        for attendee in attendees
    ]
    return JsonResponse({"attendees": attendees_list})

def api_show_attendee(request, id):
    attendee = Attendee.objects.get(pk=id)

    conference = attendee.conference
    conference_details = {
        "name": conference.name,
        "href": request.build_absolute_uri(reverse("api_show_conference", kwargs={"id": conference.id}))
    }




    attendee_details = {
        "email": attendee.email,
        "name": attendee.name,
        "company_name": attendee.company_name,
        "created": attendee.created.strftime('%Y-%m-%d %H:%M:%S'),
        "conference": conference_details,
    }
    return JsonResponse(attendee_details)
